package tradavo.miami;

import com.google.gson.Gson;

public class InventoryBase {

    public String toJSON(){
        Gson g = new Gson();
        return g.toJson(this);
    }
}
