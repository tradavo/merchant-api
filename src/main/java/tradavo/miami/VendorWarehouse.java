package tradavo.miami;

public class VendorWarehouse extends InventoryBase {
    private String sWarehouse ;
    private int iQuantity ;  // the new inventory
    private int iOldInventory ;
    private InventoryResults eStatus ;
    // used on the server side
    private String merchantDistro ;
    private String merchantAsin ;
    private String eta ;
    private String resultMsg ;

    public VendorWarehouse(){
        this.init();
    }
    public VendorWarehouse(final String whName, final String inv){
        this.init();
        this.setsWarehouse(whName);
        this.setEta("");
        try {
            this.setiQuantity(Integer.parseInt(inv));
        }catch( NumberFormatException nf ){
            this.eStatus = InventoryResults.ERROR ;
        }
    }

    public boolean equals(VendorWarehouse comp){
        if(this.getsWarehouse().equals(comp.getsWarehouse()))
            return true;
        return false ;
    }

    private void init(){
        eStatus = InventoryResults.SUCCESS ;
        this.setResultMsg("");
    }

    public String getsWarehouse() {
        return sWarehouse;
    }

    public void setsWarehouse(String sWarehouse) {
        this.sWarehouse = sWarehouse;
    }

    public int getiQuantity() {
        return iQuantity;
    }

    public String getQuantity() {
        return Integer.toString(iQuantity);
    }

    public void setiQuantity(int iQuantity) {
        this.iQuantity = iQuantity;
    }

    public InventoryResults geteStatus() {
        return eStatus;
    }

    public void seteStatus(InventoryResults eStatus) {
        this.eStatus = eStatus;
    }

    public String getMerchantDistro() {
        return merchantDistro;
    }

    public void setMerchantDistro(String merchantDistro) {
        this.merchantDistro = merchantDistro;
    }

    public String getMerchantAsin() {
        return merchantAsin;
    }

    public void setMerchantAsin(String merchantAsin) {
        this.merchantAsin = merchantAsin;
    }

    public int getOldInventory() {
        return iOldInventory;
    }

    public void setOldInventory(int iOldInventory) {
        this.iOldInventory = iOldInventory;
    }
    public String asinWithPrefix(){
        if( this.getMerchantAsin().length() == 11 )
            return "0" + this.getMerchantAsin() ;
        return this.getMerchantAsin() ;
    }

    public boolean asinMatch(final String asinIn){
        if( this.merchantAsin.equalsIgnoreCase(asinIn))
            return true ;
        return false ;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }
}
