package tradavo.miami;

public enum InventoryResults {
        SUCCESS,
        ERROR,
        NOT_FOUND,
        DISCONTINUED,
        NO_CHANGE,
        IGNORED,
        NEW
}

