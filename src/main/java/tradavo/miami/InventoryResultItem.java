package tradavo.miami;

import java.util.ArrayList;
import java.util.List;

public class InventoryResultItem extends InventoryBase{
    private String vendorItemId ;
    // used by the server side to convert vendor item id to asin and distro
    private String merchantAsin ;
    private List<InventoryResultWarehouse> warehouses = new ArrayList<>();

    public InventoryResultItem(){
        super();
        this.init();
    }
    public InventoryResultItem(VendorInventoryItem vi){
        super();
        this.init(vi);
    }

    private void init(VendorInventoryItem vi){
        this.init();
        this.setVendorItemId(vi.getVendorItemId());
        this.setMerchantAsin( vi.getMerchantAsin());
        List<VendorWarehouse> whouses = vi.getWarehouseList();
        for(VendorWarehouse vh : whouses){
            InventoryResultWarehouse irw = new InventoryResultWarehouse(vh);
            this.set(irw);
        }
    }

    private void init(){
        if( warehouses == null ){
            warehouses = new ArrayList<>();
        }
    }
    public void set(InventoryResultWarehouse wh){
        this.addWarehouse( wh );
    }

    public void addWarehouse( InventoryResultWarehouse wh){
        warehouses.add(wh);
    }

    public List<InventoryResultWarehouse> getWarehouseList(){
        return warehouses ;
    }

    public String getVendorItemId() {
        return vendorItemId;
    }

    public void setVendorItemId(String vendorItemId) {
        this.vendorItemId = vendorItemId;
    }

    public String getMerchantAsin() {
        return merchantAsin;
    }

    public void setMerchantAsin(String merchantAsin) {
        this.merchantAsin = merchantAsin;
    }
}
