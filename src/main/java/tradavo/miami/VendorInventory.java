package tradavo.miami;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class VendorInventory extends InventoryBase{

    private String vendorKey ;
    private HashMap< String,VendorInventoryItem> itemList ;

    public VendorInventory(){
        this.init();
    }
    public VendorInventory(final String vendor){
        this.init(vendor);
    }

    private void init(final String vendor){
        this.setVendorKey(vendor);
        this.init();
    }
    private void init(){
        itemList = new HashMap<>();
    }

    public int getCount(){
        return itemList.size() ;
    }

    public ItemResults addItem(VendorInventoryItem vi){
        if(itemList.containsKey(vi.getVendorItemId()))
            return ItemResults.EXISTS ;
        itemList.put(vi.getVendorItemId(), vi);
        return ItemResults.NEW ;
    }

    public VendorInventoryItem getVendorItem(final String vendorItemId){
        if( itemList.containsKey(vendorItemId))
            return itemList.get(vendorItemId);
        return null ;
    }

    public List<VendorInventoryItem> getItemList(){
        return itemList.values().stream().collect(Collectors.toList());
    }

    public String getVendorKey() {
        return vendorKey;
    }

    public void setVendorKey(String vendorKey) {
        this.vendorKey = vendorKey;
    }
}
