package tradavo.miami;

public class InventoryResultWarehouse extends InventoryBase{
    private InventoryResults result ;
    private int oldInventory ;
    private int newInventory ;
    private String message ;

    public InventoryResultWarehouse(){
        super();
    }
    public InventoryResultWarehouse(VendorWarehouse vw){
        super();
        if( vw.geteStatus() == InventoryResults.NOT_FOUND){
            this.setMessage("Warehouse " + vw.getsWarehouse() + " was not found in merchant.  Possibly inactive.");
        }else if(vw.geteStatus() == InventoryResults.IGNORED) {
            this.setMessage("Warehouse " + vw.getsWarehouse() + ": status " + vw.geteStatus().toString());
        }else
            this.setMessage("Warehouse "  + vw.getsWarehouse() +" :" + vw.getMerchantAsin() + "|"+ vw.getMerchantDistro() + " status " + vw.geteStatus().toString());
        this.setOldInventory(vw.getOldInventory());
        this.setNewInventory(vw.getiQuantity());
        this.setResult(vw.geteStatus());
    }
    public void set(InventoryResults r, String m){
        this.setMessage(m);
        this.setResult(r);
    }

    public InventoryResults getResult() {
        return result;
    }

    public void setResult(InventoryResults result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getOldInventory() {
        return oldInventory;
    }

    public void setOldInventory(int oldInventory) {
        this.oldInventory = oldInventory;
    }

    public int getNewInventory() {
        return newInventory;
    }

    public void setNewInventory(int newInventory) {
        this.newInventory = newInventory;
    }
}
