package tradavo.miami;

import tradavo.api.model.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VendorInventoryItem extends InventoryBase {
    private String vendorItemId ;
    // used by the server side to convert vendor item id to asin and distro
    private String merchantAsin ;
    private String vendor ;
    private List<VendorWarehouse> warehouseList ;

    public VendorInventoryItem(){
       this.init();
    }
    public VendorInventoryItem(final String itemId, final String vendor){
        this.setVendor(vendor);
        this.setVendorItemId(itemId);
        this.init();
    }

    private void init(){
        warehouseList = new ArrayList<>();
    }

    public InventoryResultWarehouse addWarehouse(VendorWarehouse vw){
        if(this.warehouseList == null){
            this.init();
        }
        this.updateList(vw) ;
        return new InventoryResultWarehouse(vw);
    }

    private void updateList(VendorWarehouse vw){
        for ( VendorWarehouse house : this.warehouseList){
            if( vw.equals(house)){
                this.warehouseList.remove(house);
            }
        }
        this.warehouseList.add(vw) ;
    }

    public List<VendorWarehouse> getWarehouseList() {
        return warehouseList;
    }

    public void setWarehouseList(List<VendorWarehouse> warehouseList) {
        this.warehouseList = warehouseList;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String sVendor) {
        this.vendor = sVendor;
    }

    public String getVendorItemId() {
        return vendorItemId;
    }

    public void setVendorItemId(String vendorItemId) {
        this.vendorItemId = vendorItemId;
    }

    public String getMerchantAsin() {
        return merchantAsin;
    }

    public void setMerchantAsin(String merchantAsin) {
        this.merchantAsin = merchantAsin;
    }
}
