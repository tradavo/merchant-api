package tradavo.miami.builder;

import tradavo.miami.InventoryBase;
import tradavo.miami.InventoryResults;

public class InventoryBuilder extends InventoryBase {
    private String vendorId ;
    private String message ;
    private InventoryResults result ;

    public InventoryBuilder(final String vendorId){
        setVendorId(vendorId);
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public InventoryResults getResult() {
        return result;
    }

    public void setResult(InventoryResults result) {
        this.result = result;
    }
}
