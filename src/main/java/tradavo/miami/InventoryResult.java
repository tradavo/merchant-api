package tradavo.miami;

import com.google.gson.Gson;
import tradavo.api.model.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class InventoryResult extends InventoryBase{
    private String message ;
    private String vendorId ;
    private int itemCount ;
    private List<InventoryResultItem> items ;

    public InventoryResult(){
        super();
        this.init();
    }

    private void init(){
        if( this.items == null){
            this.items = new ArrayList<>();
        }
    }

    public InventoryResult(VendorInventory v){
        super();
        this.init();
        this.setVendorId(v.getVendorKey());
        List<VendorInventoryItem> vItems = v.getItemList() ;
        for(VendorInventoryItem vi : vItems){
            InventoryResultItem iri = new InventoryResultItem(vi);
            this.items.add(iri);
        }
        this.setItemCount(this.items.size());
    }

    public List<InventoryResultItem> getItems() {
        return items;
    }

    public void addItem(InventoryResultItem ir) {
        this.items.add(ir);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }
}
