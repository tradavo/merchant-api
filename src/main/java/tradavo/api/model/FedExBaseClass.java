package tradavo.api.model;

import com.google.gson.Gson;
import tradavo.api.model.fedex.validate.AddressValidator;

import java.lang.reflect.Type;

public abstract class FedExBaseClass {
    public String toJSON(){
        final Gson gson = new Gson();
        return( gson.toJson((Object)this));
    }

    // forces the type to cast itself but that's fine
    protected Object fromJSON(final String json, final Class cls){
        final Gson gson = new Gson();
        return gson.fromJson(json, (Type) cls);
    }
}
