// 
// Decompiled by Procyon v0.5.36
// 

package tradavo.api.model;

import java.util.ArrayList;

public class MerchantShipment extends JSONObject
{
    protected String siteId ;
    protected String orderId ;
    protected String carrier ;
    protected String trackingId;
    public MerchantShipment() {
        this.setType("merchantShipment");
    }

    public MerchantShipment(final String carrier, final String orderId, final String siteId) {
        this();
        this.siteId = siteId ;
        this.orderId = orderId ;
        this.carrier = carrier ;
        this.setId(orderId + "$" + carrier + "$" + siteId);
    }

    @Override
    public void setId(String value) {
        super.setId(this.orderId + "$" + this.carrier + "$" + this.siteId);
    }

    public boolean sameOrder(final String lastOrder ){
        return lastOrder.equals(this.orderId) ;
    }
    
    public void setCarrier(final String carrier) {
        this.carrier = carrier;
        this.setKeyValuePair("carrier", carrier);
    }
    public String getCarrier(){ return this.carrier;}
    public String getOrderId(){return this.orderId;}

    public void setTracking(final String tracking) {
        this.trackingId = tracking;
        this.setKeyValuePair("trackingId", tracking);
    }
    public String getTrackingId(){
        return this.trackingId ;
    }
    public void setOrderId(final String orderID) {
        this.orderId = orderID;
        this.setKeyValuePair("order", orderID);
    }
    public void setSite(final String siteID) {
        this.siteId = siteID ;
        this.setKeyValuePair("site", siteID);
    }
    
    public final String[] parseTrackingId(){
        return ShippingConstants.parseTrackingId(this.getTrackingId()) ;
    }

    public final boolean matchCarrier( final String carrier ){
        boolean bRet = false ;
        if(this.getCarrier().equals(carrier) || this.carrier.equals(ShippingConstants.invalidCarrier)){
            bRet = true ;
        }
        return bRet ;
    }

    public final boolean matchCarrier(final String trackingId, final String carrier){
        boolean bRet = this.matchCarrier(carrier);
        if( bRet )
            bRet = ShippingConstants.isFedExTrackingNumber(trackingId);
        return bRet ;
    }

}
