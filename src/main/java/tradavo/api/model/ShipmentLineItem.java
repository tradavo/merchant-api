// 
// Decompiled by Procyon v0.5.36
// 

package tradavo.api.model;

public class ShipmentLineItem extends JSONObject
{
    public ShipmentLineItem() {
        this.setType("shipmentLineItem");
    }
    
    public ShipmentLineItem(final String value) {
        this();
        this.setId(value);
    }
    
    public void setCarrier(final String carrier) {
        this.setKeyValuePair("carrier", carrier);
    }
    
    public void setTrackingNumber(final String trackingNumber) {
        this.setKeyValuePair("trackingNumber", trackingNumber);
    }
    
    public void setQuantity(final String quantity) {
        this.setKeyValuePair("quantity", quantity);
    }
    
    public void setUnitCost(final String unitCost) {
        this.setKeyValuePair("unitCost", unitCost);
    }
    
    public void setOrder(final String orderID) {
        this.setKeyValuePair("order", orderID);
    }
    
    public void setProduct(final String productID) {
        this.setKeyValuePair("product", productID);
    }
    
    public void setSite(final String siteID) {
        this.setKeyValuePair("site", siteID);
    }
}
