package tradavo.api.model;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import tradavo.api.model.fedex.TrackDateAndTime;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;


public class ShippingConstants {
    public final static String SHIPPING_QUERY = "select shipping.shipper, shipping.tracking_id, purchased_items.ship_quantity, purchased_items.price, purchases.order_id, products.item_master_id " +
            "as product_id, users.accounts.id as site_id, purchased_items.distro " ;

    public final static String NOTIFICATION_QUERY = "select shipping.tracking_id,shipping.shipper, purchases.order_id," +
            "purchased_items.ship_quantity,purchased_items.itemid ,users.accounts.id as site_id," +
            "aa.ship_contact, aa.ship_email";

    public final static String FROM_SECTION = " from product.purchase_history_item as purchased_items" +
            " left join product.purchase_history as purchases on purchased_items.order_id = purchases.order_id and purchased_items.distro = purchases.vendor " +
            " left join users.accounts on accounts.account = purchases.account" +
            " left join product.ship_history as shipping on shipping.order_id = purchases.order_id and shipping.itemid = purchased_items.itemid and shipping.distro = purchased_items.distro" +
            " inner join product.amazon_product as products on products.distro = purchased_items.distro and products.asin = shipping.itemid ";

    public final static String NOTIFICATION_FROM = " left join users.account_address aa on aa.account = accounts.account " ;

    public final static String MAIN_WHERE = "where DATEDIFF(NOW(),purchases.purchase_time) < $1 and accounts.visuality_flag = 2";
    public final static String POST_WHERE = "where DATEDIFF(NOW(),purchases.purchase_time) < $1 and accounts.visuality_flag >= 1"; // MPB 7-20

    /*************************************************************
     * tracking specific query parts
     *************************************************************/
    public final static String TRACK_FROM = "select tracking.orderid, tracking.distro, UPPER(tracking.trackingid) as trackingid, tracking.shipper_status ,purchases.account as site_id ,purchases.purchase_time "+
    "from product.ship_tracking as tracking " +
    "left join product.purchase_history as purchases on purchases.order_id = tracking.orderid ";
    public final static String TRACK_WHERE_2 = "and trackingid NOT LIKE 'DEL%' and trackingid NOT LIKE 'SHIP%' ";
    public final static String TRACK_WHERE = "where DATEDIFF(NOW(),purchases.purchase_time) < $1 and tracking.shipper_status != 1 "; // the tracking add_on  MPB
    public final static String TRACK_GROUP = "group by tracking.trackingid " +  // MPB 8-3-2022 to ensure we get ALL the records we want.
            "order by tracking.orderid,purchases.purchase_time DESC";

    /*************************************
     * some query addons
     *************************************/
    public static final String POST_QUERY_ADDON =
           " and (tracking_id != 'Delivery' and tracking_id != '' and tracking_id != 'DELIVERED')  " +
    " group by shipping.tracking_id " +
    " order by purchases.order_id  ;";

    public final static String UPDATE_QUERY = "UPDATE product.ship_tracking " +
            "SET shipper_status=?, shipper_desc=?, shipper_date=?, received_by=?, received_location=?, expected_date=? WHERE trackingid=?;";
    public final static String ADD_STATUS = "REPLACE INTO product.ship_tracking_status " +
            "(trackingid,status_json) VALUES (?,?)";

    public final static String COMMA = "[, ]" ;
    public final static String BAR = "[|]" ;
    public final static String SPACE = "[ ]" ;

    public final static String setWhereDaysBack(final String daysToQuery, final boolean bPost){
        if( bPost)
            return POST_WHERE.replace("$1", daysToQuery);

        return MAIN_WHERE.replace("$1", daysToQuery);
    }

    // these constants will be used in this way
    // GET : SHIPPING_QUERY + FROM_SECTION + MAIN_WHERE(converted)
    // POST SHIPMENTS : SHIPPING_QUERY + FROM_SECTION + MAIN_WHERE(converted) + POST_QUERY_ADDON
    // POST NOTIFICATIONS : NOTIFICATION_QUERY + FROM_SECTION + NOTIFICATION_FROM + MAIN_WHERE(converted) + POST_QUERY_ADDON

    // for the request object
    public final static String REQUESTTYPE = "requestType" ;
    public final static String DAYS = "days";
    // used by the shipper service
    public final static String REQUESTQUERY = "requestType={$TYPE}&days={$DAYS}" ;
    public final static String SHIPMENT = "shipping" ;
    public final static String NOTIFY = "notifications" ;
    public final static String DEFAULTDAYS = "30" ;

    public final static String invalidCarrier = "Invalid";
    public final static int fedexTrackingLength = 12 ;   // the length of a fedex tracking id

    public final static String ESTIMATED_DEL = "SDD";
    public final static String RESCHEDULE_DEL = "RDD";
    // these are the things that support FEDEX return values
    private static String[] FedExResults = {
            "UNKNOWN",
            "ACTUAL_DELIVERY",
            "ACTUAL_PICKUP",
            "SHIP",
            "ACTUAL_TENDER",
            "ANTICIPATED_TENDER",
            "ESTIMATED_DELIVERY"
    };

    private static String[] MerchantResults = {
        "Created",
        "Delivered",
        "Picked Up",
        "Shipped",
        "In Process",
        "Expected Tender",
        "Estimated Delivery"
    };
    private static int ERROR = -1 ;
    private static int UNKNOWN = 0 ;
    private static int DELIVERED = 1 ;
    private static int PICKEDUP = 2 ;
    private static int SHIPPED = 3 ;
    private static int TENDERED = 4 ;
    private static int EXPECTED = 5 ;
    private static int ESTIMATED = 6 ;

    private static final String ERRORSTATUS = "Status Error" ;
    private static final String ERRORTEXT = "Error at shipper, please check later";

    private static DateTimeFormatter ddf = DateTimeFormatter.ofPattern("yyyy-MM-dd 'at' hh:mm a");
    // private static SimpleDateFormat estFormatter = SimpleDateFormat.("yyyy-MM-dd");

    public static boolean isFedExTrackingNumber(final String trackingId){
        boolean bRet = false ;
        if( trackingId == null )
            return bRet ;  // null tracking id is always not a fed ex tracking number
        bRet = trackingId.length() == fedexTrackingLength ;  // first check
        return bRet ;
    }

    public static int getFedExStatusCode( final String status){
        int iRet = 0 ;
        for( int x = 0; x <= ESTIMATED ; x++){
             if(FedExResults[x].equalsIgnoreCase(status)) {
                 iRet = x; // if the status is this entry then we have what we need
                 break;
             }
        }
        return iRet ;
    }

    public static String getFedExStatusText( final int status){
        if( status < ERROR ) {
           return ddf.format(LocalDateTime.now()) ;
        }
        if( status > ESTIMATED )
           return FedExResults[0]; // return unknown if invalid
        return FedExResults[status];
    }

    public static String getMerchantStatus( final int iStatus ){
        if( iStatus < UNKNOWN )
            return ShippingConstants.ERRORTEXT ;
        if( iStatus > ESTIMATED)
            return ShippingConstants.MerchantResults[0];
        return ShippingConstants.MerchantResults[iStatus] ;
    }

    public static String getMerchantStatus( final String status ){
        int ct = 0 ;
        for(String feStatus : FedExResults){
            if(feStatus.equalsIgnoreCase(status)){
                return MerchantResults[ct];
            }
            ct++;
        }
        return "Unknown" ;
    }

    public static String generateLocalDate( final String theDate){
        String retStr = theDate ;
        try {
            OffsetDateTime off = OffsetDateTime.parse(theDate);
            LocalDateTime date = off.atZoneSameInstant(ZoneId.systemDefault())
                    .toLocalDateTime();
            retStr = ShippingConstants.ddf.format(date);
        }catch(DateTimeParseException p){
            // swallow the exception for now
        }
        return retStr ;
    }

    /*
    public static String generateEstimatedDate( final String theDate){
        String retStr = theDate ;
        try {
            retStr = ShippingConstants.estFormatter.format(theDate);
        }catch(DateTimeParseException p){
            // swallow the exception for now
        }
        return retStr ;
    }
    */

    public static String[] parseTrackingId(final String trackingId){
        String trackID = trackingId.trim();
        // first we have to make sure they are all the same separator
        trackID = trackID.replace("|",",");
        String[] tracking_ids = null ;
        if(trackID.indexOf(",") > 0 )
            tracking_ids = trackID.split(ShippingConstants.COMMA);
        else if ( trackID.indexOf("|") > 0 )
            tracking_ids = trackID.split(ShippingConstants.BAR);
        else if ( trackID.indexOf(" ") > 0 )
            tracking_ids = trackID.split(ShippingConstants.SPACE);
        else {
            tracking_ids = new String[1] ;
            tracking_ids[0] = trackID;
        }
        return tracking_ids ;
    }

    public static String mapUPSStatus( final String upsStatus){
        String res = FedExResults[0];
        switch( upsStatus) {
            case "D": //ACTUAL_DELIVERY
                res = FedExResults[DELIVERED] ;
                break ;
            case "I":
            case "M":
                res = FedExResults[TENDERED];
                break ;
            case "P":
                res = FedExResults[PICKEDUP];
                break;
            case "O":
                res = FedExResults[EXPECTED];
                break;
            case "DD":
                res = FedExResults[SHIPPED];
                break;
            case "MV":
            case "X":
            case "RS":
                res = "ERROR" ;
                break;
            default :
                res = FedExResults[UNKNOWN];
        }
        return res ;
    }

    public static String buildUPSDescription( final String desc, final String date, final String code){
        return desc + "|" + date + "|" + code ;
    }

    // parses into description|date|code
    public static String[] parseUPSDescription( final String upsDesc){
        return upsDesc.split("\\|");
    }

    // get activities from the merchant db
    public static List<TrackDateAndTime> getActivityList(final String json) throws JsonSyntaxException{
        Gson g = new Gson() ;
        List<TrackDateAndTime> dnt = new ArrayList<>();
        Type listOfDateAndTime = new TypeToken<ArrayList<TrackDateAndTime>>() {}.getType();
        if (json != null && json.length() > 0)
            dnt = g.fromJson(json, listOfDateAndTime);
        return dnt ;
    }

    public static int LONGDATE = 1;
    public static int SHORTDATE = 0 ;
    private static DateTimeFormatter longDate = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static DateTimeFormatter longDateOut = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm");
    private static DateTimeFormatter shortDate = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter shortDateOut = DateTimeFormatter.ofPattern("MM-dd-yyyy");
    private static DateTimeFormatter timeZoneIn = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ssXXX");

    public static String formatDate(Timestamp ts){
      return ts.toLocalDateTime().format(ShippingConstants.longDateOut);
    }

    public static boolean isDelivery(final String tdtType){
        return ShippingConstants.FedExResults[ShippingConstants.DELIVERED].equals(tdtType);
    }

  public static String formatDate(final int inputType, final int outputType,final String input){
    boolean hasTime = (inputType == LONGDATE || outputType == LONGDATE) ;
    DateTimeFormatter in = inputType == LONGDATE ? longDate : shortDate ;
    DateTimeFormatter out = outputType == LONGDATE ? longDateOut : shortDateOut ;
    return formatDate(in, out,input,hasTime ) ;
  }

  public static String formatTZDate( final String input ){
      return ShippingConstants.formatTZDate(LONGDATE, input);
  }

  public static String formatTZDate( final int outType, final String input ){
        String outDate = "";
        try {
           DateTimeFormatter out = outType == LONGDATE ? longDateOut : shortDateOut;
           String zInput = input.replace("T", " ");  // remove the T
           ZonedDateTime zd = ZonedDateTime.parse(zInput, timeZoneIn);
           outDate = zd.format(out);
       }catch(Exception dtpe){
           outDate = input.substring(0,10);  // if the date is crap you get the short date....sorry about that.
           System.out.println(dtpe.getMessage()) ;
       }
        return outDate ;
    }

  public static String formatDate(final DateTimeFormatter in, final DateTimeFormatter out,final String input, final boolean hasTime){
   try{
        if(hasTime){
        	LocalDateTime dd = (LocalDateTime)LocalDateTime.parse(input, in );
            return dd.format(out);
        }else{
  		    LocalDate d = LocalDate.parse(input, in );
            return d.format(out);
        }
    }catch(Exception ex ){
        return input.substring(0,10);
    }
  }

// end of shipping constants
}


