// 
// Decompiled by Procyon v0.5.36
// 

package tradavo.api.model;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

public class JSONObject
{
    protected LinkedTreeMap<String, Object> data;
    
    public JSONObject() {
        this.data = (LinkedTreeMap<String, Object>)new LinkedTreeMap();
    }
    
    public void setType(final String value) {
        this.data.put("type", value);
    }
    
    public void setId(final String value) {
        this.data.put("id", value);
    }
    
    public String getId() {
        return (String)this.data.get((Object)"id");
    }
    
    public LinkedTreeMap<String, Object> getAttributes() {
        return (LinkedTreeMap<String, Object>)this.data.get((Object)"attributes");
    }

    public String toJSON(){
        final Gson gson = new Gson();
        return( gson.toJson((Object)this));
    }
    
    public LinkedTreeMap<String, Object> getRelationships() {
        return (LinkedTreeMap<String, Object>)this.data.get((Object)"relationships");
    }
    
    public void setAttribute(final String attribute, final Object value) {
        if (!this.data.containsKey((Object)"attributes")) {
            this.data.put("attributes", new LinkedTreeMap());
        }
        ((LinkedTreeMap)this.data.get((Object)"attributes")).put((Object)attribute, value);
    }
    
    public void setRelationship(final String relationship, final Object value) {
        if (!this.data.containsKey((Object)"relationships")) {
            this.data.put("relationships", new LinkedTreeMap());
        }
        ((LinkedTreeMap)this.data.get((Object)"relationships")).put((Object)relationship, value);
    }
    
    public void setKeyValuePair(final String key, final String value) {
        this.data.put(key, value);
    }
}
