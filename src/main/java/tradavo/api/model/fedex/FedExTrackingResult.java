package tradavo.api.model.fedex;

import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class FedExTrackingResult {
    private String trackingNumber ;
    private TrackError trackError;
    private String orderId ;
    private String description ;
    private String recipientName ;
    private String locationDelivery ;
    private String shipperResult ;
    private List<TrackDateAndTime> dateTime = new ArrayList<>();
    private EstimatedDelivery estimatedDelivery = new EstimatedDelivery();

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public List<TrackDateAndTime> getDateTime(){
        return this.dateTime ;
    }

    public TrackError getError() {
        return trackError;
    }

    public void setError(final String type, final String description) {
        if( this.trackError == null ) {
            this.trackError = new TrackError();
        }
        this.trackError.setCode(type);
        this.trackError.setMessage(description);
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TrackDateAndTime getDateTime(final int entry) {
        if(entry >= this.dateTime.size())
            return null ;
        return (TrackDateAndTime) this.dateTime.toArray()[entry];
    }

    public TrackDateAndTime getDateTime(final String sEntry){
        for(TrackDateAndTime td : this.dateTime){
            if( td.getType().equals(sEntry))
                return td;
        }
        return null;
    }

    public void setDateTime(TrackDateAndTime dateTime) {
        this.dateTime.add( dateTime );
    }

    public void setDateTime(final String type, final String dateTime, final String city, final String state){
        if( this.dateTime == null ){
            this.dateTime = new ArrayList<>();
        }
        TrackDateAndTime newOne = new TrackDateAndTime();
        newOne.setType(type);
        newOne.setDateTime( dateTime );
        newOne.setCity(city);
        newOne.setState(state);
        this.setDateTime(newOne);
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getLocationDelivery() {
        return locationDelivery;
    }

    public void setLocationDelivery(String locationDelivery) {
        this.locationDelivery = locationDelivery;
    }

    public EstimatedDelivery getEstimatedDelivery() {
        return estimatedDelivery;
    }

    public void setEstimatedDelivery(EstimatedDelivery estimatedDelivery) {
        this.estimatedDelivery = estimatedDelivery;
    }

    // write your date and time data to JSON
    public String toJson(){
        Gson g = new Gson() ;
        String json = g.toJson(this.dateTime);
        return json ;
    }

    public String getShipperResult() {
        return shipperResult;
    }

    public void setShipperResult(String shipperResult) {
        this.shipperResult = shipperResult;
    }
}
