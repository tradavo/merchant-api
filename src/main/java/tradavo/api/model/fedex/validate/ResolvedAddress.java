
package tradavo.api.model.fedex.validate;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ResolvedAddress {

    @Expose
    private Attributes attributes;
    @Expose
    private String city;
    @Expose
    private List<String> cityToken;
    @Expose
    private String classification;
    @Expose
    private String countryCode;
    @Expose
    private List<Object> customerMessage;
    @Expose
    private Boolean generalDelivery;
    @Expose
    private Boolean normalizedStatusNameDPV;
    @Expose
    private ParsedPostalCode parsedPostalCode;
    @Expose
    private Boolean postOfficeBox;
    @Expose
    private PostalCodeToken postalCodeToken;
    @Expose
    private String resolutionMethodName;
    @Expose
    private Boolean ruralRouteHighwayContract;
    @Expose
    private String standardizedStatusNameMatchSource;
    @Expose
    private String stateOrProvinceCode;
    @Expose
    private List<String> streetLinesToken;

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<String> getCityToken() {
        return cityToken;
    }

    public void setCityToken(List<String> cityToken) {
        this.cityToken = cityToken;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public List<Object> getCustomerMessage() {
        return customerMessage;
    }

    public void setCustomerMessage(List<Object> customerMessage) {
        this.customerMessage = customerMessage;
    }

    public Boolean getGeneralDelivery() {
        return generalDelivery;
    }

    public void setGeneralDelivery(Boolean generalDelivery) {
        this.generalDelivery = generalDelivery;
    }

    public Boolean getNormalizedStatusNameDPV() {
        return normalizedStatusNameDPV;
    }

    public void setNormalizedStatusNameDPV(Boolean normalizedStatusNameDPV) {
        this.normalizedStatusNameDPV = normalizedStatusNameDPV;
    }

    public ParsedPostalCode getParsedPostalCode() {
        return parsedPostalCode;
    }

    public void setParsedPostalCode(ParsedPostalCode parsedPostalCode) {
        this.parsedPostalCode = parsedPostalCode;
    }

    public Boolean getPostOfficeBox() {
        return postOfficeBox;
    }

    public void setPostOfficeBox(Boolean postOfficeBox) {
        this.postOfficeBox = postOfficeBox;
    }

    public PostalCodeToken getPostalCodeToken() {
        return postalCodeToken;
    }

    public void setPostalCodeToken(PostalCodeToken postalCodeToken) {
        this.postalCodeToken = postalCodeToken;
    }

    public String getResolutionMethodName() {
        return resolutionMethodName;
    }

    public void setResolutionMethodName(String resolutionMethodName) {
        this.resolutionMethodName = resolutionMethodName;
    }

    public Boolean getRuralRouteHighwayContract() {
        return ruralRouteHighwayContract;
    }

    public void setRuralRouteHighwayContract(Boolean ruralRouteHighwayContract) {
        this.ruralRouteHighwayContract = ruralRouteHighwayContract;
    }

    public String getStandardizedStatusNameMatchSource() {
        return standardizedStatusNameMatchSource;
    }

    public void setStandardizedStatusNameMatchSource(String standardizedStatusNameMatchSource) {
        this.standardizedStatusNameMatchSource = standardizedStatusNameMatchSource;
    }

    public String getStateOrProvinceCode() {
        return stateOrProvinceCode;
    }

    public void setStateOrProvinceCode(String stateOrProvinceCode) {
        this.stateOrProvinceCode = stateOrProvinceCode;
    }

    public List<String> getStreetLinesToken() {
        return streetLinesToken;
    }

    public void setStreetLinesToken(List<String> streetLinesToken) {
        this.streetLinesToken = streetLinesToken;
    }

}
