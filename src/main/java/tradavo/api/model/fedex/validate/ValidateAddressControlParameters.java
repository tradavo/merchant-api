
package tradavo.api.model.fedex.validate;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ValidateAddressControlParameters {

    @Expose
    private Boolean includeResolutionTokens = true;

    public Boolean getIncludeResolutionTokens() {
        return includeResolutionTokens;
    }

    public void setIncludeResolutionTokens(Boolean includeResolutionTokens) {
        this.includeResolutionTokens = includeResolutionTokens;
    }

}
