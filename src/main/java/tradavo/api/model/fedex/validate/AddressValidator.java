
package tradavo.api.model.fedex.validate;

import java.util.List;
import javax.annotation.Generated;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import tradavo.api.model.FedExBaseClass;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AddressValidator extends FedExBaseClass {
    @Expose
    private String accountId ;
    @Expose
    private List<AddressesToValidate> addressesToValidate;

    // this goes into the header for customer transaction id
    public void setAccount(final String theAccount ){
        this.accountId = theAccount ;
    }
    public String getAccount(){
        return this.accountId ;
    }

    public List<AddressesToValidate> getAddressesToValidate() {
        return addressesToValidate;
    }
    public void setAddressesToValidate(List<AddressesToValidate> addressesToValidate) {
        this.addressesToValidate = addressesToValidate;
    }

    public void addAddressToValidate( Address add){
        this.addressesToValidate.add(new AddressesToValidate(add));
    }

    public AddressValidator fromJSON(final String json){
        return (AddressValidator) super.fromJSON(json, AddressValidator.class );
    }

}
