
package tradavo.api.model.fedex.validate.results;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import tradavo.api.model.fedex.validate.Alert;
import tradavo.api.model.fedex.validate.ResolvedAddress;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Output {

    @Expose
    private List<Alert> alerts;
    @Expose
    private List<ResolvedAddress> resolvedAddresses;

    public List<Alert> getAlerts() {
        return alerts;
    }

    public void setAlerts(List<Alert> alerts) {
        this.alerts = alerts;
    }

    public List<ResolvedAddress> getResolvedAddresses() {
        return resolvedAddresses;
    }

    public void setResolvedAddresses(List<ResolvedAddress> resolvedAddresses) {
        this.resolvedAddresses = resolvedAddresses;
    }

}
