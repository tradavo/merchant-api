package tradavo.api.model.fedex.validate;

public class ValidationConstants {
    // a bunch of static stuff to ensure that we get values
    // and correct messages

    // address validation alerts
    public static String[] alerts = {"DATESTAMP.FORMAT.INVALID","COUNTRY.CODE.INVALID",
            "STANDARDIZED.ADDRESS.NOTFOUND","ACCOUNTVERIFICATION.ACCOUNT.NOTFOUND"};
    public static String[] alertMsg = {
    "We are unable to process this request. Please try again later or contact FedEx Customer Service.",
    "Specified country code {country_code} is invalid",
    "Standardized address is not found.",
    "Account not shippable."};

    public static String alertMessage(final String alert){
        String res = "Unknown alert returned " + alert ;
        for(int x = 0 ; x < alerts.length; x++){
            if( alerts[x].equalsIgnoreCase(alert))
                res = alertMsg[x];
        }
        return res ;
    }


}
