
package tradavo.api.model.fedex.validate;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Attributes {

    @SerializedName("AddressPrecision")
    private String addressPrecision;
    @SerializedName("AddressType")
    private String addressType;
    @SerializedName("CountrySupported")
    private Boolean countrySupported;
    @SerializedName("DPV")
    private Boolean dPV;
    @SerializedName("DataVintage")
    private String dataVintage;
    @SerializedName("Inserted")
    private Boolean inserted;
    @SerializedName("InvalidSuiteNumber")
    private Boolean invalidSuiteNumber;
    @SerializedName("MatchSource")
    private String matchSource;
    @SerializedName("Matched")
    private Boolean matched;
    @SerializedName("MultiUnitBase")
    private Boolean multiUnitBase;
    @SerializedName("MultipleMatches")
    private Boolean multipleMatches;
    @SerializedName("POBox")
    private Boolean pOBox;
    @SerializedName("POBoxOnlyZIP")
    private Boolean pOBoxOnlyZIP;
    @SerializedName("RRConversion")
    private Boolean rRConversion;
    @SerializedName("ResolutionInput")
    private String resolutionInput;
    @SerializedName("ResolutionMethod")
    private String resolutionMethod;
    @SerializedName("Resolved")
    private Boolean resolved;
    @SerializedName("SplitZIP")
    private String splitZIP;
    @SerializedName("StreetAddress")
    private Boolean streetAddress;
    @SerializedName("SuiteRequiredButMissing")
    private Boolean suiteRequiredButMissing;
    @SerializedName("UniqueZIP")
    private Boolean uniqueZIP;
    @SerializedName("ValidMultiUnit")
    private Boolean validMultiUnit;
    @SerializedName("ValidlyFormed")
    private Boolean validlyFormed;
    @SerializedName("ZIP11Match")
    private Boolean zIP11Match;
    @SerializedName("ZIP4Match")
    private Boolean zIP4Match;

    public String getAddressPrecision() {
        return addressPrecision;
    }

    public void setAddressPrecision(String addressPrecision) {
        this.addressPrecision = addressPrecision;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public Boolean getCountrySupported() {
        return countrySupported;
    }

    public void setCountrySupported(Boolean countrySupported) {
        this.countrySupported = countrySupported;
    }

    public Boolean getDPV() {
        return dPV;
    }

    public void setDPV(Boolean dPV) {
        this.dPV = dPV;
    }

    public String getDataVintage() {
        return dataVintage;
    }

    public void setDataVintage(String dataVintage) {
        this.dataVintage = dataVintage;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getInvalidSuiteNumber() {
        return invalidSuiteNumber;
    }

    public void setInvalidSuiteNumber(Boolean invalidSuiteNumber) {
        this.invalidSuiteNumber = invalidSuiteNumber;
    }

    public String getMatchSource() {
        return matchSource;
    }

    public void setMatchSource(String matchSource) {
        this.matchSource = matchSource;
    }

    public Boolean getMatched() {
        return matched;
    }

    public void setMatched(Boolean matched) {
        this.matched = matched;
    }

    public Boolean getMultiUnitBase() {
        return multiUnitBase;
    }

    public void setMultiUnitBase(Boolean multiUnitBase) {
        this.multiUnitBase = multiUnitBase;
    }

    public Boolean getMultipleMatches() {
        return multipleMatches;
    }

    public void setMultipleMatches(Boolean multipleMatches) {
        this.multipleMatches = multipleMatches;
    }

    public Boolean getPOBox() {
        return pOBox;
    }

    public void setPOBox(Boolean pOBox) {
        this.pOBox = pOBox;
    }

    public Boolean getPOBoxOnlyZIP() {
        return pOBoxOnlyZIP;
    }

    public void setPOBoxOnlyZIP(Boolean pOBoxOnlyZIP) {
        this.pOBoxOnlyZIP = pOBoxOnlyZIP;
    }

    public Boolean getRRConversion() {
        return rRConversion;
    }

    public void setRRConversion(Boolean rRConversion) {
        this.rRConversion = rRConversion;
    }

    public String getResolutionInput() {
        return resolutionInput;
    }

    public void setResolutionInput(String resolutionInput) {
        this.resolutionInput = resolutionInput;
    }

    public String getResolutionMethod() {
        return resolutionMethod;
    }

    public void setResolutionMethod(String resolutionMethod) {
        this.resolutionMethod = resolutionMethod;
    }

    public Boolean getResolved() {
        return resolved;
    }

    public void setResolved(Boolean resolved) {
        this.resolved = resolved;
    }

    public String getSplitZIP() {
        return splitZIP;
    }

    public void setSplitZIP(String splitZIP) {
        this.splitZIP = splitZIP;
    }

    public Boolean getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(Boolean streetAddress) {
        this.streetAddress = streetAddress;
    }

    public Boolean getSuiteRequiredButMissing() {
        return suiteRequiredButMissing;
    }

    public void setSuiteRequiredButMissing(Boolean suiteRequiredButMissing) {
        this.suiteRequiredButMissing = suiteRequiredButMissing;
    }

    public Boolean getUniqueZIP() {
        return uniqueZIP;
    }

    public void setUniqueZIP(Boolean uniqueZIP) {
        this.uniqueZIP = uniqueZIP;
    }

    public Boolean getValidMultiUnit() {
        return validMultiUnit;
    }

    public void setValidMultiUnit(Boolean validMultiUnit) {
        this.validMultiUnit = validMultiUnit;
    }

    public Boolean getValidlyFormed() {
        return validlyFormed;
    }

    public void setValidlyFormed(Boolean validlyFormed) {
        this.validlyFormed = validlyFormed;
    }

    public Boolean getZIP11Match() {
        return zIP11Match;
    }

    public void setZIP11Match(Boolean zIP11Match) {
        this.zIP11Match = zIP11Match;
    }

    public Boolean getZIP4Match() {
        return zIP4Match;
    }

    public void setZIP4Match(Boolean zIP4Match) {
        this.zIP4Match = zIP4Match;
    }

}
