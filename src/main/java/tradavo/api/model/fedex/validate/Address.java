
package tradavo.api.model.fedex.validate;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Address {

    @Expose
    private String addressVerificationId;
    @Expose
    private String city;
    @Expose
    private String countryCode;
    @Expose
    private String postalCode;
    @Expose
    private String stateOrProvinceCode;
    @Expose
    private List<String> streetLines;
    @Expose
    private String urbanizationCode;

    public String getAddressVerificationId() {
        return addressVerificationId;
    }

    public void setAddressVerificationId(String addressVerificationId) {
        this.addressVerificationId = addressVerificationId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getStateOrProvinceCode() {
        return stateOrProvinceCode;
    }

    public void setStateOrProvinceCode(String stateOrProvinceCode) {
        this.stateOrProvinceCode = stateOrProvinceCode;
    }

    public List<String> getStreetLines() {
        return streetLines;
    }

    public void setStreetLines(List<String> streetLines) {
        this.streetLines = streetLines;
    }

    public String getUrbanizationCode() {
        return urbanizationCode;
    }

    public void setUrbanizationCode(String urbanizationCode) {
        this.urbanizationCode = urbanizationCode;
    }

}
