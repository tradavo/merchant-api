
package tradavo.api.model.fedex.validate;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AddressesToValidate {

    @Expose
    private Address address;

    public AddressesToValidate(Address a){
        this.setAddress(a);
    }
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
