
package tradavo.api.model.fedex;

import tradavo.api.model.ShippingConstants;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class TrackDateAndTime
{

//("type")
    private String type;
//("dateTime")
    private String dateTime;

    private String city = "";
    private String state = "";
    private final static long serialVersionUID = 8413422922566163515L;

    public String getCityState(){
        if( this.state.length() == 0 )
            return(this.city);
        return (this.city + ", "+this.state);
    }

    public String getDateValue(final boolean isFedEx){
        String dt = this.splitDate();
        boolean bDelivered = ShippingConstants.isDelivery(this.type);
        // need to reformat this one
        if( isFedEx) {
            if( bDelivered )
                return ShippingConstants.formatTZDate(dt);
            else
                return this.getDateValue(ShippingConstants.SHORTDATE,true);
        }// it's UPS so much simpler
        return ShippingConstants.formatDate(ShippingConstants.SHORTDATE,ShippingConstants.SHORTDATE,dt);
    }

    public String getDateValue(final int outputType, final boolean isFedEx){
        String dt = this.splitDate();
        // need to reformat this one
        if( isFedEx )
            return ShippingConstants.formatTZDate(outputType,dt);
        return ShippingConstants.formatDate(ShippingConstants.SHORTDATE,outputType,dt);
    }

    public String getDescription(){

        // need to reformat this one
        return ShippingConstants.generateLocalDate(this.splitDesc());
    }
    private String splitDate(){
        String[] dateFields = dateTime.split("\\|");
        if(dateFields.length > 1)
            return dateFields[1].replace("T"," ");
        return dateFields[0];
    }
    private String splitDesc(){
        String[] dateFields = dateTime.split("\\|");
        return dateFields[0];
    }

//("type")
    public String getType() {
        return type;
    }
    public String getType(int theStatus) {
        return ShippingConstants.getMerchantStatus(theStatus);
    }
//("type")
    public void setType(String type) {
        this.type = type;
    }

//("dateTime")
    public String getDateTime() {
        return dateTime;
    }

//("dateTime")
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
