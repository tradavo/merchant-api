package tradavo.api.model.fedex;

public class EstimatedDelivery {
    private String trackingid = "";
    private String estimatedDeliveryDate ;

    public EstimatedDelivery(){
        this.trackingid = "" ;
        this.estimatedDeliveryDate = "";
    }
    public EstimatedDelivery( final String track, final String estDate){
        this.setTrackingid(track);
        this.setEstimatedDeliveryDate(estDate);
    }

    public String getTrackingid() {
        return trackingid;
    }

    public void setTrackingid(String trackingid) {
        this.trackingid = trackingid;
    }

    public String getEstimatedDeliveryDate() {
        return estimatedDeliveryDate;
    }

    public void setEstimatedDeliveryDate(String estimatedDeliveryDate) {
        this.estimatedDeliveryDate = estimatedDeliveryDate;
    }
}
