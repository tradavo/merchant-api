
package tradavo.api.model.fedex;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class TrackError
{

//("code")
    private String code;
//("message")
    private String message;
    private final static long serialVersionUID = 8211725830994945234L;

//("code")
    public String getCode() {
        return code;
    }

//("code")
    public void setCode(String code) {
        this.code = code;
    }

//("message")
    public String getMessage() {
        return message;
    }

//("message")
    public void setMessage(String message) {
        this.message = message;
    }
}
