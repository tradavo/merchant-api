package tradavo.rest.auth.result;

public class FedExAuthorizationToken implements IAuthorizationToken{
    //@JsonProperty("access_token")
    String access_token;
    //@JsonProperty("token_type")
    String token_type;
    //@JsonProperty("expires_in")
    int expires_in;
    //@JsonProperty("scope")
    String scope;

    public String getAccess_token() { return this.access_token; }
    public void setAccess_token(String access_token) { this.access_token = access_token; }
    public String getScope() { return this.scope; }
    public void setScope(String scope) { this.scope = scope; }
    public int getExpires_in() { return this.expires_in; }
    public void setExpires_in(int expires_in) { this.expires_in = expires_in; }
    public String getToken_type() { return this.token_type; }
    public void setToken_type(String token_type) { this.token_type = token_type; }

    // this are the calls that the interface specifies so that we can use implements on the base class
    public String getAuthToken(){ return this.getAccess_token();}

    public static IAuthorizationToken fromJSON(){ return new FedExAuthorizationToken();}
}
