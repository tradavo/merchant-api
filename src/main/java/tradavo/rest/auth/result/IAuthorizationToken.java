package tradavo.rest.auth.result;

public interface IAuthorizationToken {
    public String getAuthToken();
    public static IAuthorizationToken fromJSON(){return null;};
}
